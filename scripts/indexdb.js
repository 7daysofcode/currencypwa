import idb from 'idb';


const dbpromise = idb.open('test-db', 1, function(upgradedDb) {
	const keyValStore = upgradedDb.createObjectStore('keyval');
	keyValStore.put('world', 'hello world');
});