(function() {
'use strict';

const url = 'https://free.currencyconverterapi.com';

let filesToCache = [
  '/',
  '/offline.html',
  'https://free.currencyconverterapi.com/api/v5/convert?q=USD_MZN&compact=y',
  'scripts/jquery.js',
  'scripts/numeral_min.js',
  'scripts/main.js',
  'styles/inline.css',
  'styles/bootstrap.min.css',
  'images/ic_add_white_24px.svg',
  'images/ic_refresh_white_24px.svg'
]; 

const curr_fr_val = $("#CURR_FR_VAL").val();


const main = document.querySelector('main');

window.addEventListener('load', async e => {

if ('serviceWorker' in navigator){
	try {
	navigator.serviceWorker
	.register('sw.js')
	.then(function() {console.log('Service Worker registered'); }); 
	}catch(error) {
		console.log('Service Worker registration failed');
	}
}


if ('caches' in window) {

	caches.open('currencyPWA-final-3').then(function(cache) { 
  cache.keys().then(function(cachedRequests) { 
    console.log(cachedRequests); // [Request, Request]
  });
});

const dbpromise = idb.open('cache-db', 1.1, function(upgradedDb) {
	const keyValStore = upgradedDb.createObjectStore('values');
	
	for (const files of filesToCache){
	keyValStore.put(files, files);

	}
});

}
});

document.getElementById('butConvert').addEventListener('click', function() {

 updateCurrency();
  });
  
document.getElementById('butRefresh').addEventListener('click', function() {
    // Refresh all of the forecasts
  updateCurrency();
  });

async function updateCurrency() {

  const fr = $("#CURR_FR").val();
  const to = $("#CURR_TO").val();
  
    const curr_fr_val = $("#CURR_FR_VAL").val();
	
	    const currVal = $("#CURR_VAL2");
  currVal.val("");
  

  const currId = fr + "_" + to;
  const protocol = window.location.protocol.replace(/:/g,'');
  currVal.attr("placeholder", "Converting...");
	$.getJSON("https://free.currencyconverterapi.com/api/v5/convert?q=" + currId + "&compact=y",
   function(data){
      try {
       const currFrVal = parseFloat(curr_fr_val);
	   currVal.val(numeral(currFrVal * data[currId].val).format("0,0.00[0]"));
	   
     } catch (e) {
      alert("Please enter a number in the Amount field.");
    }

    currVal.attr("placeholder", "Press Convert button");

  });
} 

})();
