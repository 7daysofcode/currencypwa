let cacheName = 'currencyPWA-final-1';

let filesToCache = [
  '/',
  '/offline.html',
  'https://free.currencyconverterapi.com/api/v5/convert?q=USD_MZN&compact=y',
  'scripts/jquery.js',
  'scripts/numeral_min.js',
  'scripts/main.js',
  'styles/inline.css',
  'styles/bootstrap.min.css',
  'images/ic_add_white_24px.svg',
  'images/ic_refresh_white_24px.svg'
]; 


self.addEventListener('install', function(e) {
console.log('[ServiceWorker] Install');
e.waitUntil(
caches.open(cacheName).then(function(cache) {
console.log('[ServiceWorker] Caching app shell');
return cache.addAll(filesToCache);
    })

  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  return self.clients.claim();
});
					  

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.open(cacheName).then(function(cache) {
      return cache.match(event.request).then(function (response) {
        return response || fetch(event.request).then(function(response) {
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});
